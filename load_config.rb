require 'yaml'

#where might the file be located?

#get the local directory
LOCAL_DIR = File.expand_path( File.dirname(__FILE__) )

conf = "getreel.yaml"

config_locs = []
config_locs << File.join(LOCAL_DIR, conf)
config_locs << File.join( File.expand_path("~/"),".config", conf)
config_locs << File.join( "/etc/", conf)

#loop through the possible config locations
config_locs.each do |cl|
	#does the config file exist?
	if File.exists? cl
		#parse the config
		@config = YAML.load_file( cl )
		break
	end
end

if @config
	#validate the Download date
	time = @config['Download']['time']
	unless time.class.name=="String" && time.match(/^[0-9]{2}:[0-9]{2}$/)
		puts "Config Download time is formatted incorrectly"
		exit
	end
else
	puts "Config is missing"
	exit
end

