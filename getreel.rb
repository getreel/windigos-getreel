#!/usr/bin/env ruby

####
# Copyright 2014 Jezra

#		This program is free software: you can redistribute it and/or modify
#		it under the terms of the GNU General Public License as published by
#		the Free Software Foundation, either version 3 of the License, or
#		(at your option) any later version.
#
#		This program is distributed in the hope that it will be useful,
#		but WITHOUT ANY WARRANTY; without even the implied warranty of
#		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
#		GNU General Public License for more details.
#
#		You should have received a copy of the GNU General Public License
#		along with this program.	If not, see <http://www.gnu.org/licenses/>.
#
####

require 'webrick'
require 'json'
require_relative 'load_config'
require_relative 'wrapper'

#add SVG support to WEBrick
WEBrick::HTTPUtils::DefaultMimeTypes.store 'svg', 'image/svg+xml'

#create a hash to store target information
@dl_targets={}
#create a list of wrappers
@wrapper_list = []
#keep track of the last download time, start with it in the past
@previous_download_time = Time.new(2001,2,3)

def create(request, response)
	response['Content-Type'] = 'text/plain'
	unless request.request_method == "POST"
		response.status = 400
		response.body = "bad request"
	else
		url = request.query['url']
		title = request.query['title']
		@dl_targets[url]=title
		response.body = "Added: #{url}"
	end
end

def videos(request, response)
	response['Content-Type'] = 'application/json'
	response.body = JSON::dump(@dl_targets)
end

def status(request, response)
	statuses = {}
	@wrapper_list.each do |w|
		statuses[w.url]=w.status
	end
	response['Content-Type'] = 'application/json'
	if statuses.empty?
		response.body = ""
	else
		response.body = JSON::dump(statuses)
	end
end

def delete(request, response)
	response['Content-Type'] = 'text/plain'
	unless request.request_method == "POST"
		response.status = 400
		response.body = "bad request"
	else
		url = request.query['url']
		if @dl_targets.delete(url)
			response.body = "deleted: #{url}"
		else
			response.status = 409
			response.body = "#{url} is not in the list"
		end
	end
end

def run_download_processor
	#when do we start paying attention?
	almost_24_hours = @previous_download_time + 86280
	#create a new thread
	Thread.new do
		#loop for endless
		while true
			#What time is it?
			now = Time.now()
			hour_minute_time = now.strftime("%H:%M")
			#compare the times as strings in 24-hour format
			if (hour_minute_time <=> @config['Download']['time']) == 0
				#has it been almost 24 hours since the previous_download_datetime
				if now >= almost_24_hours
					#reset the previous_download_time
					@previous_download_time = now
					almost_24_hours = @previous_download_time + 86280

					#loop through the dl_targets and create a new wrapper
					@dl_targets.each do |url, title|
						puts "Downloading: #{url}"
						@wrapper_list << Wrapper.new( url, @config['Download']['directory'], title)
					end
				end
			end
			#sleep for a bit
			sleep 30
		end
	end
end

# Initialize our WEBrick server
if $0 == __FILE__
	#get the server config from config
	sc = @config['Server']
	port = sc['port'].to_i
	public_dir = sc['public_dir']
	server = WEBrick::HTTPServer.new({:Port=>port,:DocumentRoot=>public_dir})
	#what urls do we need to mount?

	server.mount_proc('/create') do |req, resp|
		create( req, resp )
	end
	#TODO: handle editing a specific video

	server.mount_proc('/videos') do |req, resp|
		videos( req, resp )
	end

	server.mount_proc('/status') do |req, resp|
		status( req, resp )
	end

	server.mount_proc('/delete') do |req, resp|
		delete( req, resp )
	end

	trap "INT" do
		server.shutdown
	end

	#run the download processor
	run_download_processor

	#start the server
	server.start

end
