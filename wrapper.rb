
# we need to get the interactive output
require "open3"
require 'thread'

class Wrapper

	attr_reader :url, :status

	#TODO add youtube-dl options to initialize
	def initialize( url ,download_dir, title=nil)
		#TODO: validate input
		@url = url
		@status = ""
		####create the command to run
		#is there a title to use?
		if title and not title.empty?
			t = '-o "#{title}.%(ext)s" '
		else
			t = '-o "%(title)s.%(ext)s" '
		end
		wrap_command = "cd #{download_dir} && youtube-dl --newline #{t}#{url}"
		puts wrap_command
		Open3.popen3( wrap_command ) do |i, o, e, t|
			o.each do |line|
				@status = line
			end
		end
	end
end
