#!/usr/bin/env ruby

require 'net/http'

#require the config loader
require_relative 'load_config'

BASE_URL = "http://localhost:#{@config['Server']['port']}"

def display_help
	#display some help
	puts " -- API Tester --"
	puts " ACTION: create "
	puts "creates a new entry based on supplied URL and optional TITLE"
	puts "#{__FILE__} create URL [TITLE]"
	puts "example: "
	puts "#{__FILE__} create https://www.youtube.com/watch?v=SMmCn8Z9rss \"Talco - St. Pauli - Live\""
	puts ""
	puts " ACTION: videos"
	puts "returns a listing of video entries"
	puts "#{__FILE__} videos [URL]"
	puts "example: "
	puts "#{__FILE__} videos "
	puts "#{__FILE__} videos https://www.youtube.com/watch?v=SMmCn8Z9rss"
	puts ""
	puts " ACTION: delete"
	puts "delete a video entry based on supplied URL"
	puts "#{__FILE__} delete URL"
	puts "example: "
	puts "#{__FILE__} delete https://www.youtube.com/watch?v=SMmCn8Z9rss"
	puts ""
	puts " ACTION: status"
	puts "get the status of all of the wrappers"
	puts "#{__FILE__} status"
	puts ""
end

def parse_action(action, target = nil)
	action+= "/#{target}}" if target
	parsed_url = URI.parse("#{BASE_URL}/#{action}")
	p parsed_url.inspect
	parsed_url
end

if ARGV[0] == "-h" || ARGV[0] == "help" || ARGV[0].nil?
	display_help
	exit
end

target = ARGV[1] if ARGV[1]
title = ARGV[2] if ARGV[2]

#create the request data
case ARGV[0]
when 'create'
	data = {'url' => target, 'title' => title}
	#what URL do to we need to call?
	url = parse_action("create")
	resp, data = Net::HTTP.post_form(url, data)
when 'videos'
	#what URL do to we need to call?
	url = parse_action("videos", target)
	resp, data = Net::HTTP.get(url)
when 'delete'
	data = {'url' => target}
	#what URL do to we need to call?
	url = parse_action("delete")
	resp, data = Net::HTTP.post_form(url, data)
when 'status'
	#what URL do to we need to call?
	url = parse_action("status")
	resp, data = Net::HTTP.get(url)
end

puts resp.inspect

