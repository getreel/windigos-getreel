#GetReel
##What?
GetReel is a network accessible wrapper for youtube-dl that performs downloads at a specific time.

##Why?
If you had a Satellite ISP, you wouldn't ask why

##Using GetReel

1. copy getreel.yaml.tmp to getreel.yaml and edit appropriately 
2. run ./getreel.rb
4. sent HTTP requests to the machine running GetReel
5. wait....
6. watch videos
